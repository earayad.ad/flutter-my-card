import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.teal,
        body: SafeArea(
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget> [
                Container(
                  child: Column(
                    children: <Widget>[
                      CircleAvatar(
                        radius: 50.0,
                        backgroundColor: Colors.white,
                        backgroundImage: AssetImage('assets/images/profile.png'),
                      ),
                      Text(
                        'Felipe A. D.',
                        style: TextStyle(
                          fontFamily: 'Pacifico',
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.bold,
                          fontSize: 24,
                          color: Colors.white
                        ),
                      ),
                      Text(
                        'FLUTTER DEVELOPER',
                        style: TextStyle(
                          fontFamily: 'Sans-Pro',
                          fontSize: 14,
                          color: Colors.white,
                          letterSpacing: 2.5,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      SizedBox(
                        width: 150,
                        child: Divider(
                          color: Colors.teal.shade100,
                        ),
                      ),
                    ],
                  )
                ),
                Card(
                  child: ListTile(
                    leading: Icon(
                      Icons.phone,
                      color: Colors.teal,
                      semanticLabel: 'Phone',
                    ),
                    title: Text('+55 11 99999-9999',
                        style: TextStyle(
                          color: Colors.teal.shade900,
                        )
                    ),
                    ),
                  color: Colors.white,
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              ),
                Card(
                  child: ListTile(
                    leading: Icon(
                      Icons.email,
                      color: Colors.teal,
                      size: 24,
                      semanticLabel: 'Email',
                    ),
                    title: Text('angela@email.com',
                        style: TextStyle(
                          color: Colors.teal.shade900,
                        )
                    ),
                  ),
                  color: Colors.white,
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

